package ObjectMaps;
//
// Imports have been added for UFT object descriptors
import com.hp.lft.sdk.GeneralLeanFtException;
import com.hp.lft.sdk.Desktop;
import com.hp.lft.sdk.web.Browser;
import com.hp.lft.sdk.web.BrowserFactory;
import com.hp.lft.sdk.web.BrowserType;

import com.hp.lft.sdk.wpf.Window;
import com.hp.lft.sdk.wpf.WindowDescription;
import com.hp.lft.sdk.wpf.Button;
import com.hp.lft.sdk.wpf.ButtonDescription;
import com.hp.lft.sdk.wpf.ComboBox;
import com.hp.lft.sdk.wpf.ComboBoxDescription;
import com.hp.lft.sdk.wpf.TabStrip;
import com.hp.lft.sdk.wpf.TabStripDescription;
import com.hp.lft.sdk.wpf.CheckBox;
import com.hp.lft.sdk.wpf.CheckBoxDescription;
public class MainWindow{

// Object Map definition
// id = MainWindow
// new variable name = MainWindow
// SilkTest Xpath =  /WPFWindow[@caption='MainWindow']
//Possible attributes:-
// Properties for type WPFWindow
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .Index .IsEnabled .IsFocused .IsModal 
// .IsVisible .Location .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri .WindowTitleRegExp 
// 
public static com.hp.lft.sdk.wpf.Window  MainWindow = create_MainWindow();

// Object Map definition
// id = Button1
// new variable name = MainWindow_Button1
// SilkTest Xpath =  //WPFButton[@automationId='Button1']
//Possible attributes:-
// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.Button  MainWindow_Button1 = create_MainWindow_Button1();

// Object Map definition
// id = Button2
// new variable name = MainWindow_Button2
// SilkTest Xpath =  //WPFButton[@automationId='Button2']
//Possible attributes:-
// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.Button  MainWindow_Button2 = create_MainWindow_Button2();

// Object Map definition
// id = Button4
// new variable name = MainWindow_Button4
// SilkTest Xpath =  //WPFToggleButton[@automationId='Button4']
//Possible attributes:-
// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.Button  MainWindow_Button4 = create_MainWindow_Button4();

// Object Map definition
// id = Button5
// new variable name = MainWindow_Button5
// SilkTest Xpath =  //WPFButton[@automationId='Button5']
//Possible attributes:-
// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.Button  MainWindow_Button5 = create_MainWindow_Button5();

// Object Map definition
// id = Button6
// new variable name = MainWindow_Button6
// SilkTest Xpath =  //WPFButton[@automationId='Button6']
//Possible attributes:-
// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.Button  MainWindow_Button6 = create_MainWindow_Button6();

// Object Map definition
// id = ComboBox1
// new variable name = MainWindow_ComboBox1
// SilkTest Xpath =  //WPFComboBox[@automationId='ComboBox1']
//Possible attributes:-
// Properties for type WPFComboBox
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Items .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .SelectedItem 
// .Size .Text .Vri .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.ComboBox  MainWindow_ComboBox1 = create_MainWindow_ComboBox1();

// Object Map definition
// id = TabControl1
// new variable name = MainWindow_TabControl1
// SilkTest Xpath =  //WPFTabControl[@automationId='TabControl1']
//Possible attributes:-
// Properties for type WPFTabControl
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .Index .IsEnabled .IsFocused .IsVisible 
// .Location .Name .NativeClass .ObjectName .ObjectProperties .SelectedTab .Size .Tabs .Text .Vri .WindowTitleRegExp 
// 
public static com.hp.lft.sdk.wpf.TabStrip  MainWindow_TabControl1 = create_MainWindow_TabControl1();

// Object Map definition
// id = toggleButton
// new variable name = MainWindow_toggleButton
// SilkTest Xpath =  //WPFToggleButton[@automationId='toggleButton']
//Possible attributes:-
// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.Button  MainWindow_toggleButton = create_MainWindow_toggleButton();

// Object Map definition
// id = zzz
// new variable name = MainWindow_zzz
// SilkTest Xpath =  //WPFButton[@automationId='Button7']/WPFCheckBox[@caption='zzz']
//Possible attributes:-
// Properties for type WPFCheckBox
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsChecked .IsEnabled 
// .IsFocused .IsThreeState .IsVisible .Location .NativeClass .ObjectName .ObjectProperties .ParentText 
// .Size .State .Text .Vri .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.CheckBox  MainWindow_zzz = create_MainWindow_zzz();


// (4) STM: Object Map Definition
// XPath: /WPFWindow[@caption='MainWindow']
	private static com.hp.lft.sdk.wpf.Window  create_MainWindow(){
		try {
			return Desktop.describe(com.hp.lft.sdk.wpf.Window.class, new com.hp.lft.sdk.wpf.WindowDescription.Builder()
				.windowTitleRegExp("MainWindow")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFButton[@automationId='Button1']
	private static com.hp.lft.sdk.wpf.Button  create_MainWindow_Button1(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
				.objectName("Button1")
				.text("Button 1")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFButton[@automationId='Button2']
	private static com.hp.lft.sdk.wpf.Button  create_MainWindow_Button2(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
				.objectName("Button2")
				.text("Show Child Window")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFToggleButton[@automationId='Button4']
	private static com.hp.lft.sdk.wpf.Button  create_MainWindow_Button4(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
				.objectName("Button4")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFButton[@automationId='Button5']
	private static com.hp.lft.sdk.wpf.Button  create_MainWindow_Button5(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
				.objectName("Button5")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFButton[@automationId='Button6']
	private static com.hp.lft.sdk.wpf.Button  create_MainWindow_Button6(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
				.objectName("Button6")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFComboBox[@automationId='ComboBox1']
	private static com.hp.lft.sdk.wpf.ComboBox  create_MainWindow_ComboBox1(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.ComboBox.class, new com.hp.lft.sdk.wpf.ComboBoxDescription.Builder()
				.objectName("ComboBox1")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFTabControl[@automationId='TabControl1']
	private static com.hp.lft.sdk.wpf.TabStrip  create_MainWindow_TabControl1(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.TabStrip.class, new com.hp.lft.sdk.wpf.TabStripDescription.Builder()
				.objectName("TabControl1")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFToggleButton[@automationId='toggleButton']
	private static com.hp.lft.sdk.wpf.Button  create_MainWindow_toggleButton(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
				.objectName("Button4")
				.text("Toggle Button")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFButton[@automationId='Button7']/WPFCheckBox[@caption='zzz']
	private static com.hp.lft.sdk.wpf.CheckBox  create_MainWindow_zzz(){
		try {
			return MainWindow.describe(com.hp.lft.sdk.wpf.CheckBox.class, new com.hp.lft.sdk.wpf.CheckBoxDescription.Builder()
					.objectName("zzz")
					.text("zzz")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

}
