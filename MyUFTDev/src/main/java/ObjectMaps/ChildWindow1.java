package ObjectMaps;
//
// Imports have been added for UFT object descriptors
import com.hp.lft.sdk.GeneralLeanFtException;
import com.hp.lft.sdk.Desktop;
import com.hp.lft.sdk.web.Browser;
import com.hp.lft.sdk.web.BrowserFactory;
import com.hp.lft.sdk.web.BrowserType;

import com.hp.lft.sdk.wpf.Window;
import com.hp.lft.sdk.wpf.WindowDescription;
import com.hp.lft.sdk.wpf.Button;
import com.hp.lft.sdk.wpf.ButtonDescription;
public class ChildWindow1{

// Object Map definition
// id = ChildWindow1
// new variable name = ChildWindow1
// SilkTest Xpath =  /WPFWindow[@caption='ChildWindow1']
//Possible attributes:-
// Properties for type WPFWindow
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .Index .IsEnabled .IsFocused .IsModal 
// .IsVisible .Location .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri .WindowTitleRegExp 
// 
public static com.hp.lft.sdk.wpf.Window  ChildWindow1 = create_ChildWindow1();

// Object Map definition
// id = button1
// new variable name = ChildWindow1_button1
// SilkTest Xpath =  //WPFButton[@automationId='button1']
//Possible attributes:-
// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.Button  ChildWindow1_button1 = create_ChildWindow1_button1();

// Object Map definition
// id = button2
// new variable name = ChildWindow1_button2
// SilkTest Xpath =  //WPFButton[@automationId='button2']
//Possible attributes:-
// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
public static com.hp.lft.sdk.wpf.Button  ChildWindow1_button2 = create_ChildWindow1_button2();


// (4) STM: Object Map Definition
// XPath: /WPFWindow[@caption='ChildWindow1']
	private static com.hp.lft.sdk.wpf.Window  create_ChildWindow1(){
		try {
			return Desktop.describe(com.hp.lft.sdk.wpf.Window.class, new com.hp.lft.sdk.wpf.WindowDescription.Builder()
				.windowTitleRegExp("ChildWindow1")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFButton[@automationId='button1']
	private static com.hp.lft.sdk.wpf.Button  create_ChildWindow1_button1(){
		try {
			return ChildWindow1.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
				.objectName("button1")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


// (4) STM: Object Map Definition
// XPath: //WPFButton[@automationId='button2']
	private static com.hp.lft.sdk.wpf.Button  create_ChildWindow1_button2(){
		try {
			return ChildWindow1.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
				.objectName("button2")
				.build());
		} catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

}
