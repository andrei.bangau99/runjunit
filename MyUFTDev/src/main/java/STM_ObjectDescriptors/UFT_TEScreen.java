package STM_ObjectDescriptors;

import com.hp.lft.sdk.te.Field;
import com.hp.lft.sdk.te.Screen;
import com.hp.lft.sdk.te.TextScreen;

public class UFT_TEScreen {

	public static Screen getParentScreen(Screen obj) {

		try {

			return  obj;

		}
		catch (Exception e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}

	}

	public static Screen getParentScreen(TextScreen obj) {

		try {

			return (Screen) obj.getParent();

		}
		catch (Exception e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}

	}

	public static Screen getParentScreen(Field obj) {

		try{

		//TextScreen ts = (TextScreen) obj.getParent();
		//return (Screen) ts.getParent();

		return (Screen) obj.getParent();

		}
		catch (Exception e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}

	}

}
