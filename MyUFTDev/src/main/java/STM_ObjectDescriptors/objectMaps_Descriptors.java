package STM_ObjectDescriptors;

//---- Imports ----
import com.hp.lft.sdk.GeneralLeanFtException;
import com.hp.lft.sdk.Desktop;

public class objectMaps_Descriptors
{

//---- Variables ----

// STM ToDo create descriptor for com.hp.lft.sdk.te.Window
private static com.hp.lft.sdk.te.Window teWindow;


// STM: Variable for class objectMaps
// //WPFWindow[@caption='MainWindow']
public static com.hp.lft.sdk.wpf.Window  MainWindow_ = describe_MainWindow_();
// //WPFButton[@automationId='Button1']
	public static com.hp.lft.sdk.wpf.Button  MainWindow_Button1 = describe_MainWindow_Button1();

// //WPFButton[@automationId='Button2']
	public static com.hp.lft.sdk.wpf.Button  MainWindow_Button2 = describe_MainWindow_Button2();

// //WPFToggleButton[@automationId='Button4']
	public static com.hp.lft.sdk.wpf.UiObject  MainWindow_Button4 = describe_MainWindow_Button4();

// //WPFButton[@automationId='Button5']
	public static com.hp.lft.sdk.wpf.Button  MainWindow_Button5 = describe_MainWindow_Button5();

// //WPFButton[@automationId='Button6']
	public static com.hp.lft.sdk.wpf.Button  MainWindow_Button = describe_MainWindow_Button();

// //WPFComboBox[@automationId='ComboBox1']
	public static com.hp.lft.sdk.wpf.ComboBox  MainWindow_Combobox1 = describe_MainWindow_Combobox1();

// //WPFPasswordBox[@automationId='PasswordBox1']
	public static com.hp.lft.sdk.wpf.UiObject  MainWindow_Passwordbox1 = describe_MainWindow_Passwordbox1();

// //WPFTabControl[@automationId='TabControl1']
	public static com.hp.lft.sdk.wpf.TabStrip  MainWindow_Tabcontrol1 = describe_MainWindow_Tabcontrol1();

// //WPFTextBox[@automationId='TextBox1']
	public static com.hp.lft.sdk.wpf.EditField  MainWindow_Textbox1 = describe_MainWindow_Textbox1();

// //WPFToggleButton[@automationId='toggleButton']
	public static com.hp.lft.sdk.wpf.UiObject  MainWindow_Togglebutton = describe_MainWindow_Togglebutton();

// //WPFButton[@automationId='Button7']/WPFCheckBox[@caption='zzz']
	public static com.hp.lft.sdk.wpf.CheckBox  MainWindow_Zzz = describe_MainWindow_Zzz();


// public static com.hp.lft.sdk.web.Browser browserBaseState = setBrowser();

//---- Descriptors ----
// public static com.hp.lft.sdk.web.Browser setBrowser() {
//  {
// return com.hp.lft.sdk.web.BrowserFactory.launch(com.hp.lft.sdk.web.BrowserType.CHROME);
// } catch (GeneralLeanFtException e) {
// TODO STM created catch block
// e.printStackTrace();
// return null;
// }
// }

//-- STM UFT Raw (New) Object Description for Window --
//-- //WPFWindow[@caption='MainWindow']


// Properties for type WPFWindow
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .Index .IsEnabled .IsFocused .IsModal 
// .IsVisible .Location .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri .WindowTitleRegExp 
// 
	public static com.hp.lft.sdk.wpf.Window describe_MainWindow_()
	{
		try{
		return Desktop.describe(com.hp.lft.sdk.wpf.Window.class, new com.hp.lft.sdk.wpf.WindowDescription.Builder()
			.text("MainWindow")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for Button --
//-- //WPFButton[@automationId='Button1']


// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
	public static com.hp.lft.sdk.wpf.Button describe_MainWindow_Button1()
	{
		try{
		return MainWindow_.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
			.objectName("Button1")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for Button --
//-- //WPFButton[@automationId='Button2']


// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
	public static com.hp.lft.sdk.wpf.Button describe_MainWindow_Button2()
	{
		try{
		return MainWindow_.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
			.objectName("Button2")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for UiObject --
//-- //WPFToggleButton[@automationId='Button4']


// Properties for type WPFToggleButton
// Properties for type UiObject
// .AbsoluteLocation, .AccessibleName, aAttachedText, .HorizontalScroll, .Index, .IsEnabled, .IsFocused, .IsRightAligned,
// .IsRightToLeftLayout, .IsRightToLeftReading, .IsVisible, .Location, .NativeClass, .Size, .Text, .VerticalScroll, .Vri
	public static com.hp.lft.sdk.wpf.UiObject describe_MainWindow_Button4()
	{
		try{
			return com.hp.lft.sdk.Desktop.describe(com.hp.lft.sdk.wpf.Window.class,null)
			.describe(com.hp.lft.sdk.wpf.UiObject.class, new com.hp.lft.sdk.wpf.UiObjectDescription.Builder()
			.objectName("Button4")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for Button --
//-- //WPFButton[@automationId='Button5']


// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
	public static com.hp.lft.sdk.wpf.Button describe_MainWindow_Button5()
	{
		try{
		return MainWindow_.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
			.objectName("Button5")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for Button --
//-- //WPFButton[@automationId='Button6']


// Properties for type WPFButton
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
	public static com.hp.lft.sdk.wpf.Button describe_MainWindow_Button()
	{
		try{
		return MainWindow_.describe(com.hp.lft.sdk.wpf.Button.class, new com.hp.lft.sdk.wpf.ButtonDescription.Builder()
			.objectName("Button6")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for ComboBox --
//-- //WPFComboBox[@automationId='ComboBox1']


// Properties for type WPFComboBox
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsEnabled .IsFocused 
// .IsVisible .Items .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .SelectedItem 
// .Size .Text .Vri .WindowTitleRegExp 
	public static com.hp.lft.sdk.wpf.ComboBox describe_MainWindow_Combobox1()
	{
		try{
		return MainWindow_.describe(com.hp.lft.sdk.wpf.ComboBox.class, new com.hp.lft.sdk.wpf.ComboBoxDescription.Builder()
			.objectName("ComboBox1")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for UiObject --
//-- //WPFPasswordBox[@automationId='PasswordBox1']


// Properties for type WPFPasswordBox
// Properties for type UiObject
// .AbsoluteLocation, .AccessibleName, aAttachedText, .HorizontalScroll, .Index, .IsEnabled, .IsFocused, .IsRightAligned,
// .IsRightToLeftLayout, .IsRightToLeftReading, .IsVisible, .Location, .NativeClass, .Size, .Text, .VerticalScroll, .Vri
	public static com.hp.lft.sdk.wpf.UiObject describe_MainWindow_Passwordbox1()
	{
		try{
			return com.hp.lft.sdk.Desktop.describe(com.hp.lft.sdk.wpf.Window.class,null)
			.describe(com.hp.lft.sdk.wpf.UiObject.class, new com.hp.lft.sdk.wpf.UiObjectDescription.Builder()
			.objectName("PasswordBox1")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for TabStrip --
//-- //WPFTabControl[@automationId='TabControl1']


// Properties for type WPFTabControl
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .Index .IsEnabled .IsFocused .IsVisible 
// .Location .Name .NativeClass .ObjectName .ObjectProperties .SelectedTab .Size .Tabs .Text .Vri .WindowTitleRegExp 
// 
	public static com.hp.lft.sdk.wpf.TabStrip describe_MainWindow_Tabcontrol1()
	{
		try{
		return MainWindow_.describe(com.hp.lft.sdk.wpf.TabStrip.class, new com.hp.lft.sdk.wpf.TabStripDescription.Builder()
			.objectName("TabControl1")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for EditField --
//-- //WPFTextBox[@automationId='TextBox1']


// Properties for type WPFTextBox
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .Index .IsEnabled .IsFocused .IsReadOnly 
// .IsVisible .Location .Name .NativeClass .ObjectName .ObjectProperties .ParentText .Size .Text .Vri 
// .WindowTitleRegExp 
	public static com.hp.lft.sdk.wpf.EditField describe_MainWindow_Textbox1()
	{
		try{
		return MainWindow_.describe(com.hp.lft.sdk.wpf.EditField.class, new com.hp.lft.sdk.wpf.EditFieldDescription.Builder()
			.objectName("TextBox1")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for UiObject --
//-- //WPFToggleButton[@automationId='toggleButton']


// Properties for type WPFToggleButton
// Properties for type UiObject
// .AbsoluteLocation, .AccessibleName, aAttachedText, .HorizontalScroll, .Index, .IsEnabled, .IsFocused, .IsRightAligned,
// .IsRightToLeftLayout, .IsRightToLeftReading, .IsVisible, .Location, .NativeClass, .Size, .Text, .VerticalScroll, .Vri
	public static com.hp.lft.sdk.wpf.UiObject describe_MainWindow_Togglebutton()
	{
		try{
			return com.hp.lft.sdk.Desktop.describe(com.hp.lft.sdk.wpf.Window.class,null)
			.describe(com.hp.lft.sdk.wpf.UiObject.class, new com.hp.lft.sdk.wpf.UiObjectDescription.Builder()
			.objectName("toggleButton")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}

//-- STM UFT Raw (New) Object Description for CheckBox --
//-- //WPFButton[@automationId='Button7']/WPFCheckBox[@caption='zzz']


// Properties for type WPFCheckBox
// .AbsoluteLocation .AttachedText .CanFocus .FullNamePath .FullType .HelpText .Index .IsChecked .IsEnabled 
// .IsFocused .IsThreeState .IsVisible .Location .NativeClass .ObjectName .ObjectProperties .ParentText 
// .Size .State .Text .Vri .WindowTitleRegExp 
	public static com.hp.lft.sdk.wpf.CheckBox describe_MainWindow_Zzz()
	{
		try{
		return MainWindow_.describe(com.hp.lft.sdk.wpf.CheckBox.class, new com.hp.lft.sdk.wpf.CheckBoxDescription.Builder()
			.text("zzz")
			.build());
		}
		catch (GeneralLeanFtException e) {
			// TODO STM created catch block
			e.printStackTrace();
			return null;
		}
	}


}
