package MigratedClasses;
// (1) STM SilkTest: import com.borland.silk.keyworddriven.annotations.Keyword;

// (3) STM SilkTest: import com.borland.silk.keyworddriven.annotations.KeywordGroup;
// (4) STM Desktop Import: import com.borland.silktest.jtf.Desktop;
// (5) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFWindow;
// (6) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFTabControl;
// (7) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFToggleButton;
// (8) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFComboBox;

import com.hp.lft.sdk.Desktop;
import com.hp.lft.sdk.FunctionKeys;
import com.hp.lft.sdk.Aut;
import ObjectMaps.MainWindow;

// @KeywordGroup("NewGroup")
// STM: KeywordGroup: @KeywordGroup("NewGroup")
public class newGroup {

// (13) STM SilkTest Desktop variable 'desktop'
private static Desktop desktop = new Desktop();


// @Keyword("Created from keyword")
// Keyword mapped to created_from_keyword
// STM: Keyword: @Keyword("Created from keyword")
public static void created_from_keyword()  throws Exception {
// TODO: Record or implement this keyword

//start recording
// (19) STM Updating: desktop.<WPFWindow>find("MainWindow").activate();
// STM: Removed: desktop.<WPFWindow>find as UFT will return an object.
// updated activate object from ("MainWindow")
MainWindow.MainWindow.activate();
// (20) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Other");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (34) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Other");
MainWindow.MainWindow_TabControl1.select("Other");
// (21) STM Updating: desktop.<WPFToggleButton>find("MainWindow.toggleButton").check();
// STM: Removed: desktop.<WPFToggleButton>find as UFT will return an object.
// STM: if object type = ToggelButtin then insert .getToggleButton() before .set
// (38) STM Replacing: MainWindow.toggleButton with MainWindow.MainWindow_toggleButton
// updated #2: ("MainWindow.toggleButton").set(com.hp.lft.sdk.CheckedState.CHECKED);
MainWindow.MainWindow_toggleButton.getToggleButton().set(com.hp.lft.sdk.CheckedState.CHECKED);
// (22) STM Updating: desktop.<WPFComboBox>find("MainWindow.ComboBox1").select("Item2");
// STM: Removed: desktop.<WPFComboBox>find as UFT will return an object.
// (41) STM Replacing: MainWindow.ComboBox1 with MainWindow.MainWindow_ComboBox1
// updated #2: ("MainWindow.ComboBox1").select("Item2");
MainWindow.MainWindow_ComboBox1.select("Item2");
// (23) STM Updating: desktop.<WPFToggleButton>find("MainWindow.toggleButton").check();
// STM: Removed: desktop.<WPFToggleButton>find as UFT will return an object.
// STM: if object type = ToggelButtin then insert .getToggleButton() before .set
// (45) STM Replacing: MainWindow.toggleButton with MainWindow.MainWindow_toggleButton
// updated #2: ("MainWindow.toggleButton").set(com.hp.lft.sdk.CheckedState.CHECKED);
MainWindow.MainWindow_toggleButton.getToggleButton().set(com.hp.lft.sdk.CheckedState.CHECKED);
// (24) STM Updating: desktop.<WPFComboBox>find("MainWindow.ComboBox1").select("Item4");
// STM: Removed: desktop.<WPFComboBox>find as UFT will return an object.
// (48) STM Replacing: MainWindow.ComboBox1 with MainWindow.MainWindow_ComboBox1
// updated #2: ("MainWindow.ComboBox1").select("Item4");
MainWindow.MainWindow_ComboBox1.select("Item4");
// (25) STM Updating: desktop.<WPFToggleButton>find("MainWindow.toggleButton").check();
// STM: Removed: desktop.<WPFToggleButton>find as UFT will return an object.
// STM: if object type = ToggelButtin then insert .getToggleButton() before .set
// (52) STM Replacing: MainWindow.toggleButton with MainWindow.MainWindow_toggleButton
// updated #2: ("MainWindow.toggleButton").set(com.hp.lft.sdk.CheckedState.CHECKED);
MainWindow.MainWindow_toggleButton.getToggleButton().set(com.hp.lft.sdk.CheckedState.CHECKED);
// (26) STM Updating: desktop.<WPFComboBox>find("MainWindow.ComboBox1").select("Item6");
// STM: Removed: desktop.<WPFComboBox>find as UFT will return an object.
// (55) STM Replacing: MainWindow.ComboBox1 with MainWindow.MainWindow_ComboBox1
// updated #2: ("MainWindow.ComboBox1").select("Item6");
MainWindow.MainWindow_ComboBox1.select("Item6");
// (27) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Calendar");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (58) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Calendar");
MainWindow.MainWindow_TabControl1.select("Calendar");
//end recording
}
}
