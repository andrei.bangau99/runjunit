package MigratedClasses;
// (1) STM BaseState Declaration: 
// (1) STM SilkTest: import com.borland.silktest.jtf.BaseState;
import org.junit.Before;
import org.junit.Test;
// (4) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFWindow;
// (5) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFTabControl;
// (6) STM Desktop Import: import com.borland.silktest.jtf.Desktop;
// (7) STM SilkTest: import com.borland.util.StringUtils;
// (8) STM SilkTest: import com.borland.silk.keyworddriven.annotations.Keyword;
// (9) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFButton;
// (10) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFToggleButton;
// (11) STM SilkTest: import com.microfocus.silktest.jtf.wpf.WPFCheckBox;

import com.hp.lft.sdk.Desktop;
import com.hp.lft.sdk.FunctionKeys;
import com.hp.lft.sdk.GeneralLeanFtException;
import com.hp.lft.sdk.Aut;
import ObjectMaps.MainWindow;
import ObjectMaps.ChildWindow1;

import unittesting.UnitTestClassBase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class simple extends UnitTestClassBase{

// (15) STM SilkTest Desktop variable 'desktop'
private static Desktop desktop = new Desktop();


@BeforeClass
public static void setUpBeforeClass() throws Exception {
  instance = new simple();
  globalSetup(simple.class);
}
//
@AfterClass
public static void tearDownAfterClass() throws Exception {
  globalTearDown();
}
  
@Before
public void baseState() {
// (19) STM BaseState Declaration: 

//===================================================================
// enable in script for JUnit
// * Add  'extends UnitTestClassBase' to 'your Class' and update your Class as below:
// enable in script for JUnit
// @BeforeClass
// public static void setUpBeforeClass() throws Exception {
//     instance = new 'your Class'();
//     globalSetup('your Class'.class);
// }
// 
// @AfterClass
// public static void tearDownAfterClass() throws Exception {
//     globalTearDown();
// }
//
// The following imports will be required...
// unittesting.UnitTestClassBase;
// org.junit.After;
// org.junit.AfterClass;
// org.junit.Before;
// org.junit.BeforeClass;
// org.junit.Test;
//===================================================================

// ToDo BaseState can be replaced by: Desktop.launchAut("path to your app");
// https://admhelp.microfocus.com/uftdev/en/2021-2023/HelpCenter/Content/HowTo/TestObjects_Manual.htm#Run
// or
// Process p = Runtime.getRuntime().exec("path to your app");
// (20) STM BaseState Execute: baseState.execute(desktop);
}

@Test
public void method1() throws Exception {
// (24) STM Updating: desktop.<WPFWindow>find("MainWindow").activate();
// STM: Removed: desktop.<WPFWindow>find as UFT will return an object.
// updated activate object from ("MainWindow")
MainWindow.MainWindow.activate();
// (25) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Tree And Checkboxes");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (64) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Tree And Checkboxes");
MainWindow.MainWindow_TabControl1.select("Tree And Checkboxes");
// (26) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Buttons");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (67) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Buttons");
MainWindow.MainWindow_TabControl1.select("Buttons");
// (27) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Data Pager");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (70) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Data Pager");
MainWindow.MainWindow_TabControl1.select("Data Pager");
// (28) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Other");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (73) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Other");
MainWindow.MainWindow_TabControl1.select("Other");
// (29) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Data Grid");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (76) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Data Grid");
MainWindow.MainWindow_TabControl1.select("Data Grid");
// (30) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Calendar");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (79) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Calendar");
MainWindow.MainWindow_TabControl1.select("Calendar");
}


// @Keyword(value = "Start application", isBaseState = true)
// Keyword mapped to start_application
// STM: Keyword: @Keyword(value = "Start application", isBaseState = true)
public static void start_application()  throws Exception {
// (36) STM BaseState Declaration: 
// ToDo BaseState can be replaced by: Desktop.launchAut("path to your app");
// https://admhelp.microfocus.com/uftdev/en/2021-2023/HelpCenter/Content/HowTo/TestObjects_Manual.htm#Run
// or
// Process p = Runtime.getRuntime().exec("path to your app");
// (37) STM BaseState Execute: baseState.execute(desktop);
}


// @Keyword("Buttons 1")
// Keyword mapped to keyword_buttons
// STM: Keyword: @Keyword("Buttons 1")
public static void keyword_buttons()  throws Exception {
// (41) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Buttons");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (102) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Buttons");
MainWindow.MainWindow_TabControl1.select("Buttons");
// (42) STM Updating: desktop.<WPFButton>find("MainWindow.Button1").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (105) STM Replacing: MainWindow.Button1 with MainWindow.MainWindow_Button1
// updated #2: ("MainWindow.Button1").select();
MainWindow.MainWindow_Button1.click();
// (43) STM Updating: desktop.<WPFButton>find("MainWindow.Button2").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (108) STM Replacing: MainWindow.Button2 with MainWindow.MainWindow_Button2
// updated #2: ("MainWindow.Button2").select();
MainWindow.MainWindow_Button2.click();
// (44) STM Updating: desktop.<WPFButton>find("ChildWindow1.button1").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (111) STM Replacing: ChildWindow1.button1 with ChildWindow1.ChildWindow1_button1
// updated #2: ("ChildWindow1.button1").select();
ChildWindow1.ChildWindow1_button1.click();
// (45) STM Updating: desktop.<WPFToggleButton>find("MainWindow.Button4").check();
// STM: Removed: desktop.<WPFToggleButton>find as UFT will return an object.
// STM: if object type = ToggelButtin then insert .getToggleButton() before .set
// (115) STM Replacing: MainWindow.Button4 with MainWindow.MainWindow_Button4
// updated #2: ("MainWindow.Button4").set(com.hp.lft.sdk.CheckedState.CHECKED);
MainWindow.MainWindow_Button4.getToggleButton().set(com.hp.lft.sdk.CheckedState.CHECKED);
// (46) STM Updating: desktop.<WPFCheckBox>find("MainWindow.zzz").check();
// STM: Removed: desktop.<WPFCheckBox>find as UFT will return an object.
// STM: if object type = ToggelButtin then insert .getToggleButton() before .set
// (119) STM Replacing: MainWindow.zzz with MainWindow.MainWindow_zzz
// updated #2: ("MainWindow.zzz").set(com.hp.lft.sdk.CheckedState.CHECKED);
MainWindow.MainWindow_zzz.set(com.hp.lft.sdk.CheckedState.CHECKED);
// (47) STM Updating: desktop.<WPFButton>find("MainWindow.Button5").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (122) STM Replacing: MainWindow.Button5 with MainWindow.MainWindow_Button5
// updated #2: ("MainWindow.Button5").select();
MainWindow.MainWindow_Button5.click();
// (48) STM Updating: desktop.<WPFButton>find("MainWindow.Button6").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (125) STM Replacing: MainWindow.Button6 with MainWindow.MainWindow_Button6
// updated #2: ("MainWindow.Button6").select();
MainWindow.MainWindow_Button6.click();
// (49) STM Updating: desktop.<WPFButton>find("MainWindow.Button2").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (128) STM Replacing: MainWindow.Button2 with MainWindow.MainWindow_Button2
// updated #2: ("MainWindow.Button2").select();
MainWindow.MainWindow_Button2.click();
// (50) STM Updating: desktop.<WPFButton>find("ChildWindow1.button2").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (131) STM Replacing: ChildWindow1.button2 with ChildWindow1.ChildWindow1_button2
// updated #2: ("ChildWindow1.button2").select();
ChildWindow1.ChildWindow1_button2.click();
// (51) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Calendar");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (134) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Calendar");
MainWindow.MainWindow_TabControl1.select("Calendar");
}


// @Keyword("Press Buttons")
// Keyword mapped to buttons_1
// STM: Keyword: @Keyword("Press Buttons")
public static void buttons_1()  throws Exception {
// (56) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Buttons");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (144) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Buttons");
MainWindow.MainWindow_TabControl1.select("Buttons");
// (57) STM Updating: desktop.<WPFButton>find("MainWindow.Button1").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (147) STM Replacing: MainWindow.Button1 with MainWindow.MainWindow_Button1
// updated #2: ("MainWindow.Button1").select();
MainWindow.MainWindow_Button1.click();
// (58) STM Updating: desktop.<WPFButton>find("MainWindow.Button2").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (150) STM Replacing: MainWindow.Button2 with MainWindow.MainWindow_Button2
// updated #2: ("MainWindow.Button2").select();
MainWindow.MainWindow_Button2.click();
// (59) STM Updating: desktop.<WPFButton>find("ChildWindow1.button1").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (153) STM Replacing: ChildWindow1.button1 with ChildWindow1.ChildWindow1_button1
// updated #2: ("ChildWindow1.button1").select();
ChildWindow1.ChildWindow1_button1.click();
// (60) STM Updating: desktop.<WPFToggleButton>find("MainWindow.Button4").check();
// STM: Removed: desktop.<WPFToggleButton>find as UFT will return an object.
// STM: if object type = ToggelButtin then insert .getToggleButton() before .set
// (157) STM Replacing: MainWindow.Button4 with MainWindow.MainWindow_Button4
// updated #2: ("MainWindow.Button4").set(com.hp.lft.sdk.CheckedState.CHECKED);
MainWindow.MainWindow_Button4.getToggleButton().set(com.hp.lft.sdk.CheckedState.CHECKED);
// (61) STM Updating: desktop.<WPFCheckBox>find("MainWindow.zzz").check();
// STM: Removed: desktop.<WPFCheckBox>find as UFT will return an object.
// STM: if object type = ToggelButtin then insert .getToggleButton() before .set
// (161) STM Replacing: MainWindow.zzz with MainWindow.MainWindow_zzz
// updated #2: ("MainWindow.zzz").set(com.hp.lft.sdk.CheckedState.CHECKED);
MainWindow.MainWindow_zzz.set(com.hp.lft.sdk.CheckedState.CHECKED);
// (62) STM Updating: desktop.<WPFButton>find("MainWindow.Button5").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (164) STM Replacing: MainWindow.Button5 with MainWindow.MainWindow_Button5
// updated #2: ("MainWindow.Button5").select();
MainWindow.MainWindow_Button5.click();
// (63) STM Updating: desktop.<WPFButton>find("MainWindow.Button6").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (167) STM Replacing: MainWindow.Button6 with MainWindow.MainWindow_Button6
// updated #2: ("MainWindow.Button6").select();
MainWindow.MainWindow_Button6.click();
// (64) STM Updating: desktop.<WPFButton>find("MainWindow.Button2").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (170) STM Replacing: MainWindow.Button2 with MainWindow.MainWindow_Button2
// updated #2: ("MainWindow.Button2").select();
MainWindow.MainWindow_Button2.click();
// (65) STM Updating: desktop.<WPFButton>find("ChildWindow1.button2").select();
// STM: Removed: desktop.<WPFButton>find as UFT will return an object.
// (173) STM Replacing: ChildWindow1.button2 with ChildWindow1.ChildWindow1_button2
// updated #2: ("ChildWindow1.button2").select();
ChildWindow1.ChildWindow1_button2.click();
// (66) STM Updating: desktop.<WPFTabControl>find("MainWindow.TabControl1").select("Calendar");
// STM: Removed: desktop.<WPFTabControl>find as UFT will return an object.
// (176) STM Replacing: MainWindow.TabControl1 with MainWindow.MainWindow_TabControl1
// updated #2: ("MainWindow.TabControl1").select("Calendar");
MainWindow.MainWindow_TabControl1.select("Calendar");
}



}
