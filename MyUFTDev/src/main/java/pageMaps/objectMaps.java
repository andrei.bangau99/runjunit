package pageMaps;
import STM_ObjectDescriptors.objectMaps_Descriptors;
import com.hp.lft.sdk.Desktop;
import com.hp.lft.sdk.FunctionKeys;
import com.hp.lft.sdk.Aut;

public class objectMaps {



// #0 STM: public String MainWindow_ = "/WPFWindow[@caption='MainWindow']";
// STM: created descriptor for: //WPFWindow[@caption='MainWindow']
// public String MainWindow_ = "//WPFWindow[@caption='MainWindow']";
////  Window MainWindow_ = objectMaps_Descriptors.describe_MainWindow_();
// STM: replaced var,//WPFWindow[@caption='MainWindow'],objectMaps_Descriptors.MainWindow_
public com.hp.lft.sdk.wpf.Window MainWindow_ = objectMaps_Descriptors.MainWindow_;

// #0 STM: public String MainWindow_Button1 = "//WPFButton[@automationId='Button1']";
// STM: created descriptor for: //WPFButton[@automationId='Button1']
// public String MainWindow_Button1 = "//WPFButton[@automationId='Button1']";
////  Button MainWindow_Button1 = objectMaps_Descriptors.describe_MainWindow_Button1();
// STM: replaced var,//WPFButton[@automationId='Button1'],objectMaps_Descriptors.MainWindow_Button1
public com.hp.lft.sdk.wpf.Button MainWindow_Button1 = objectMaps_Descriptors.MainWindow_Button1;

// #0 STM: public String MainWindow_Button2 = "//WPFButton[@automationId='Button2']";
// STM: created descriptor for: //WPFButton[@automationId='Button2']
// public String MainWindow_Button2 = "//WPFButton[@automationId='Button2']";
////  Button MainWindow_Button2 = objectMaps_Descriptors.describe_MainWindow_Button2();
// STM: replaced var,//WPFButton[@automationId='Button2'],objectMaps_Descriptors.MainWindow_Button2
public com.hp.lft.sdk.wpf.Button MainWindow_Button2 = objectMaps_Descriptors.MainWindow_Button2;

// #0 STM: public String MainWindow_Button4 = "//WPFToggleButton[@automationId='Button4']";
// STM: created descriptor for: //WPFToggleButton[@automationId='Button4']
// public String MainWindow_Button4 = "//WPFToggleButton[@automationId='Button4']";
////  UiObject MainWindow_Button4 = objectMaps_Descriptors.describe_MainWindow_Button4();
// STM: replaced var,//WPFToggleButton[@automationId='Button4'],objectMaps_Descriptors.MainWindow_Button4
public com.hp.lft.sdk.wpf.UiObject MainWindow_Button4 = objectMaps_Descriptors.MainWindow_Button4;

// #0 STM: public String MainWindow_Button5 = "//WPFButton[@automationId='Button5']";
// STM: created descriptor for: //WPFButton[@automationId='Button5']
// public String MainWindow_Button5 = "//WPFButton[@automationId='Button5']";
////  Button MainWindow_Button5 = objectMaps_Descriptors.describe_MainWindow_Button5();
// STM: replaced var,//WPFButton[@automationId='Button5'],objectMaps_Descriptors.MainWindow_Button5
public com.hp.lft.sdk.wpf.Button MainWindow_Button5 = objectMaps_Descriptors.MainWindow_Button5;

// #0 STM: public String MainWindow_Button = "//WPFButton[@automationId='Button6']";
// STM: created descriptor for: //WPFButton[@automationId='Button6']
// public String MainWindow_Button = "//WPFButton[@automationId='Button6']";
////  Button MainWindow_Button = objectMaps_Descriptors.describe_MainWindow_Button();
// STM: replaced var,//WPFButton[@automationId='Button6'],objectMaps_Descriptors.MainWindow_Button
public com.hp.lft.sdk.wpf.Button MainWindow_Button = objectMaps_Descriptors.MainWindow_Button;

// #0 STM: public String MainWindow_Combobox1 = "//WPFComboBox[@automationId='ComboBox1']";
// STM: created descriptor for: //WPFComboBox[@automationId='ComboBox1']
// public String MainWindow_Combobox1 = "//WPFComboBox[@automationId='ComboBox1']";
////  ComboBox MainWindow_Combobox1 = objectMaps_Descriptors.describe_MainWindow_Combobox1();
// STM: replaced var,//WPFComboBox[@automationId='ComboBox1'],objectMaps_Descriptors.MainWindow_Combobox1
public com.hp.lft.sdk.wpf.ComboBox MainWindow_Combobox1 = objectMaps_Descriptors.MainWindow_Combobox1;

// #0 STM: public String  MainWindow_Passwordbox1 = "//WPFPasswordBox[@automationId='PasswordBox1']";
// STM: created descriptor for: //WPFPasswordBox[@automationId='PasswordBox1']
// public String  MainWindow_Passwordbox1 = "//WPFPasswordBox[@automationId='PasswordBox1']";
////  UiObject MainWindow_Passwordbox1 = objectMaps_Descriptors.describe_MainWindow_Passwordbox1();
// STM: replaced var,//WPFPasswordBox[@automationId='PasswordBox1'],objectMaps_Descriptors.MainWindow_Passwordbox1
public com.hp.lft.sdk.wpf.UiObject  MainWindow_Passwordbox1 = objectMaps_Descriptors.MainWindow_Passwordbox1;

// #0 STM: public String MainWindow_Tabcontrol1 = "//WPFTabControl[@automationId='TabControl1']";
// STM: created descriptor for: //WPFTabControl[@automationId='TabControl1']
// public String MainWindow_Tabcontrol1 = "//WPFTabControl[@automationId='TabControl1']";
////  TabStrip MainWindow_Tabcontrol1 = objectMaps_Descriptors.describe_MainWindow_Tabcontrol1();
// STM: replaced var,//WPFTabControl[@automationId='TabControl1'],objectMaps_Descriptors.MainWindow_Tabcontrol1
public com.hp.lft.sdk.wpf.TabStrip MainWindow_Tabcontrol1 = objectMaps_Descriptors.MainWindow_Tabcontrol1;

// #0 STM: public String MainWindow_Textbox1 = "//WPFTextBox[@automationId='TextBox1']";
// STM: created descriptor for: //WPFTextBox[@automationId='TextBox1']
// public String MainWindow_Textbox1 = "//WPFTextBox[@automationId='TextBox1']";
////  EditField MainWindow_Textbox1 = objectMaps_Descriptors.describe_MainWindow_Textbox1();
// STM: replaced var,//WPFTextBox[@automationId='TextBox1'],objectMaps_Descriptors.MainWindow_Textbox1
public com.hp.lft.sdk.wpf.EditField MainWindow_Textbox1 = objectMaps_Descriptors.MainWindow_Textbox1;

// #0 STM: public String MainWindow_Togglebutton = "//WPFToggleButton[@automationId='toggleButton']";
// STM: created descriptor for: //WPFToggleButton[@automationId='toggleButton']
// public String MainWindow_Togglebutton = "//WPFToggleButton[@automationId='toggleButton']";
////  UiObject MainWindow_Togglebutton = objectMaps_Descriptors.describe_MainWindow_Togglebutton();
// STM: replaced var,//WPFToggleButton[@automationId='toggleButton'],objectMaps_Descriptors.MainWindow_Togglebutton
public com.hp.lft.sdk.wpf.UiObject MainWindow_Togglebutton = objectMaps_Descriptors.MainWindow_Togglebutton;

// #0 STM: public String MainWindow_Zzz = "//WPFButton[@automationId='Button7']/WPFCheckBox[@caption='zzz']";
// STM: created descriptor for: //WPFButton[@automationId='Button7']/WPFCheckBox[@caption='zzz']
// public String MainWindow_Zzz = "//WPFButton[@automationId='Button7']/WPFCheckBox[@caption='zzz']";
////  CheckBox MainWindow_Zzz = objectMaps_Descriptors.describe_MainWindow_Zzz();
// STM: replaced var,//WPFButton[@automationId='Button7']/WPFCheckBox[@caption='zzz'],objectMaps_Descriptors.MainWindow_Zzz
public com.hp.lft.sdk.wpf.CheckBox MainWindow_Zzz = objectMaps_Descriptors.MainWindow_Zzz;


}
