package KeywordDrivenTests;
// Imports have been added for UFT object descriptors & junit
// com.hp.lft.sdk.* can be updated if required.
import com.hp.lft.sdk.*;
import com.hp.lft.sdk.GeneralLeanFtException;
import unittesting.UnitTestClassBase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
// STM: added imports

//
// Imports have been added for UFT object descriptors
import com.hp.lft.sdk.GeneralLeanFtException;

import MigratedClasses.newGroup;
import MigratedClasses.simple;


public class Buttons extends UnitTestClassBase
{


//--------------- STM 
// Test case annotations
	@BeforeClass
		public static void setUpBeforeClass() throws Exception {
		instance = new Buttons();
		globalSetup(Buttons.class);
	}

	@AfterClass
		public static void tearDownAfterClass() throws Exception {
		globalTearDown();
	}

	@Before
		public void setUp() throws Exception {
	}

	@After
		public void tearDown() throws Exception {
	}
	@Test
	public void kdt_Buttons () throws Exception
	{
	// call to method: Start application
	//STM: - unable to match 
	//Start application
	// call to method: Press Buttons
	simple.buttons_1();
	// call to method: Buttons 1
	simple.keyword_buttons();
	// call to method: Created from keyword
//	newGroup.created_from_keyword();
	}
}
