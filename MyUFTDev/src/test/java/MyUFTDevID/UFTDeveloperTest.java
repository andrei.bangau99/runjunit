package MyUFTDevID;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hp.lft.sdk.*;
import com.hp.lft.sdk.web.Browser;
import com.hp.lft.sdk.web.BrowserFactory;
import com.hp.lft.sdk.web.BrowserType;
import com.hp.lft.sdk.web.Button;
import com.hp.lft.sdk.web.ButtonDescription;
import com.hp.lft.sdk.web.EditField;
import com.hp.lft.sdk.web.EditFieldDescription;
import com.hp.lft.sdk.web.Image;
import com.hp.lft.sdk.web.ImageDescription;
import com.hp.lft.sdk.web.Menu;
import com.hp.lft.sdk.web.MenuDescription;
import unittesting.*;


public class UFTDeveloperTest extends UnitTestClassBase {

    public UFTDeveloperTest() {
        //Change this constructor to private if you supply your own public constructor
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        instance = new UFTDeveloperTest();
        globalSetup(UFTDeveloperTest.class);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        globalTearDown();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() throws GeneralLeanFtException {
		Browser browser = BrowserFactory.launch(BrowserType.FIREFOX);

		browser.navigate("http://lnz-sctmdaily.microfocus.com:19120/login");

		EditField clientUsernameEditField = browser.describe(EditField.class, new EditFieldDescription.Builder()
				.name("userName")
				.tagName("INPUT")
				.type("text").build());
		clientUsernameEditField.setValue("admin");

		EditField passwordEditField = browser.describe(EditField.class, new EditFieldDescription.Builder()
				.name("passWord")
				.tagName("INPUT")
				.type("password").build());
		passwordEditField.setSecure("6576ea0daa7b9074240ba71c15a5391c");

		Button loginButton = browser.describe(Button.class, new ButtonDescription.Builder()
				.buttonType("submit")
				.name("Login")
				.tagName("BUTTON").build());
		loginButton.click();

		Menu mainMenuLEFTMenu = browser.describe(Menu.class, new MenuDescription.Builder()
				.id("mainMenuLEFT")
				.tagName("DIV").build());
		mainMenuLEFTMenu.select("Tests");

    }
    
    @Test
    public void test2() throws GeneralLeanFtException {
		Browser browser = BrowserFactory.launch(BrowserType.FIREFOX);

		browser.navigate("http://lnz-sctmdaily.microfocus.com:19120/silk/DEF/TM/Dashboard");

		Menu mainMenuLEFTMenu = browser.describe(Menu.class, new MenuDescription.Builder()
				.id("mainMenuLEFT")
				.tagName("DIV").build());
		mainMenuLEFTMenu.select("Tracking");

		mainMenuLEFTMenu.select("Issues");

		mainMenuLEFTMenu.select("Home");

		Image refreshImage = browser.describe(Image.class, new ImageDescription.Builder()
				.alt("")
				.tagName("IMG")
				.type(com.hp.lft.sdk.web.ImageType.LINK)
				.index(1).build());
		refreshImage.click();

		mainMenuLEFTMenu.select("Tests");


    }

}